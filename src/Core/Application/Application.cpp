#include <Core/Application/Application.h>
#include <Core/Model/CubeChunk.h>
#include <yaml-cpp/yaml.h>
#include <easylogging++.h>
#include <glm/glm.hpp>
#include <Core/Utility/FpsCounter.h>
#include <Core/Model/Skybox.h>
#include <Core/Model/Picker.h>
#include <Core/Model/Cube.h>
#include <BulletDynamics/Character/btKinematicCharacterController.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include <BulletCollision/CollisionShapes/btCapsuleShape.h>

Application::Application() {
    YAML::Node config = YAML::LoadFile("config.yaml");
    width = config["width"].as<float>();
    height = config["height"].as<float>();
    title = config["title"].as<std::string>();
    v_sync = config["v_sync"].as<int>();
    AA_value = config["v_sync"].as<int>();
}

int Application::Run() {
    try {
        LOG(INFO) << "Application started";
        window = GetWindow(width, height, title.c_str());
        SceneManager* scene = new SceneManager(width,height);
        SetListener(scene);
        glfwSetKeyCallback(window, (GLFWkeyfun)scene->KeyboardCallback);
        glfwSetScrollCallback(window, scene->scroll_callback);
        glfwSetCursorPosCallback(window, scene->cursor_position_callback);
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
//        glfwSetFramebufferSizeCallback(window, ResizeWindow);
        glfwSwapInterval(v_sync); // no v-sync = 0 , v-sync = 1
        glEnable(GL_CULL_FACE);
        glCullFace(GL_FRONT);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LESS);
        glEnable(GL_STENCIL_TEST);

        AddSkybox(scene);
       // AddPicker(scene);
        AddChunks(scene, 1, 1);
        AddLineX(scene);
        AddLineY(scene);
        AddLineZ(scene);
        AddCube(scene, 1, 1, 32, "superCube6");
        AddCube(scene, 1, 15, 228, "superCube7");
        AddCube(scene, 15, 15, 256, "superCube8");
        AddCube(scene, 15, 1, 324, "superCube9");

        auto counter = FpsCounter();

        while (!glfwWindowShouldClose(window)) {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
            if (listener)
            {
                listener->NotifyBeginFrame(counter.fps);
                listener->NotifyDisplayFrame();

                Update();
                listener->NotifyEndFrame();
                counter.showFPS(window);
            }
        }
    }
    catch (...) {
        auto ex = std::current_exception();
        std::cout << "Unknown exception" << std::endl;
        std::cin.get();
        throw;
    }
    return 0;
}

GLFWwindow *Application::GetWindow(int width, int height, const char *title) {
    /* Initialize the library */
    if (!glfwInit())
        throw std::runtime_error("glfwInit");

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    glfwWindowHint(GLFW_SAMPLES, AA_value); //anti-aliasing MSAA

    /* Create a windowed mode window and its OpenGL context */
    auto window = glfwCreateWindow(width, height, title, NULL, NULL);
    if (!window) {
        glfwTerminate();
        throw std::runtime_error("Window was not created");
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);

    /* Version */
    std::cout << glGetString(GL_VERSION) << std::endl;
    return window;
}

void Application::Update() {
    /* Swap front and back buffers */
    glfwSwapBuffers(window);
    /* Poll for and process events */
    glfwPollEvents();
}

void Application::SetListener(IListener *listener) {
    this->listener = listener;
}

void Application::AddChunks(SceneManager *scene, int x, int y) {
    for(int i = 0; i < x; i++){
        for (int j = 0; j < y; j++) {
            CubeChunk* chunk = new CubeChunk();
            chunk->SetProgram(ShaderManager::GetShader("colorShader"));
            chunk->Create(16,16,16,glm::vec3(32*i, 16*j, 0));
            auto name = "chunk" + std::to_string(i) + std::to_string(j);
            scene->GetModels_Manager()->SetModel(name, chunk);
        }
    }
}

void Application::AddSkybox(SceneManager *scene){
    Skybox *skybox = new Skybox();
    skybox->SetProgram(ShaderManager::GetShader("skyboxShader"));
    skybox->Create();
    scene->GetModels_Manager()->SetModel("_skybox", skybox);
};


void Application::AddPicker(SceneManager *scene){
    Picker *picker = new Picker();
    scene->GetModels_Manager()->SetModel("picker", picker);
}

//void Application::ResizeWindow(GLFWwindow* window, int width, int height) {
//    glViewport(0, 0, width, height);
//    float aspect=(float)width/(float)height;
//}

void Application::AddLineY(SceneManager *scene){
    auto createAction = [](Line *l){
        l->vertices = glm::mat2x3{
                0, 0, 0 + 10,
                0, 1, 0 + 10,
        };
        l->colors = glm::mat2x4{
                1, 0, 0, 1,
                1, 0, 0, 1,
        };
    };
    auto drawAction = [](Line *l){
        float time = glfwGetTime();
        float max = std::max(time/2, 1.0f);
        l->translatedVertices = glm::mat2x3{
                l->vertices[0].x, l->vertices[0].y + max, l->vertices[0].z,
                l->vertices[1].x, l->vertices[1].y + max, l->vertices[1].z,
        };
    };
    AddLine(scene, createAction,drawAction, "lineY");
}

void Application::AddLineX(SceneManager *scene) {
    auto createAction = [](Line *l){
        l->vertices = glm::mat2x3{
                0, 0, 0 + 10,
                1, 0, 0 + 10,
        };
        l->colors = glm::mat2x4{
                0, 1, 0, 1,
                0, 1, 0, 1,
        };
    };
    auto drawAction = [](Line *l){
        float time = glfwGetTime();
        float max = std::max(time/2, 1.0f);
        l->translatedVertices = glm::mat2x3{
                l->vertices[0].x + max, l->vertices[0].y, l->vertices[0].z,
                l->vertices[1].x + max, l->vertices[1].y, l->vertices[1].z,
        };
    };
    AddLine(scene, createAction,drawAction, "lineX");

}

void Application::AddLine(SceneManager *scene, std::function<void(Line *line)> createAction,
                          std::function<void(Line *line)> drawAction, std::string name) {
    Line *line = new Line();
    line->SetProgram(ShaderManager::GetShader("lineShader"));
    line->Create(createAction, drawAction);
    scene->GetModels_Manager()->SetModel(name, line);
}

void Application::AddLineZ(SceneManager *scene) {
    auto createAction = [](Line *l){
        l->vertices = glm::mat2x3{
                0, 0, 0 + 10,
                0, 0, 1 + 10,
        };
        l->colors = glm::mat2x4{
                0, 0, 1, 1,
                0, 0, 1, 1,
        };
    };
    auto drawAction = [](Line *l){
        float time = glfwGetTime();
        float max = std::max(time/2, 1.0f);
        l->translatedVertices = glm::mat2x3{
                l->vertices[0].x, l->vertices[0].y, l->vertices[0].z + max,
                l->vertices[1].x, l->vertices[1].y, l->vertices[1].z + max,
        };
    };
    AddLine(scene, createAction,drawAction, "lineZ");
}

void Application::AddCube(SceneManager *scene, int x, int y, int z, std::string name) {
    auto ptr = scene->GetPhysicsManager()->CreateCube(glm::vec3(x,y,z));
    auto cube = new Cube();
    cube->Create(ptr);
    cube->SetProgram(ShaderManager::GetShader("colorShader"));
    scene->GetModels_Manager()->SetModel( name,cube);
}
