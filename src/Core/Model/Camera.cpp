#include <Core/Model/Camera.h>
#include <glm/detail/type_mat.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <easylogging++.h>

Camera::Camera() {
    //cameraPositionEyeVector = glm::vec3(34, 18, 9);
    cameraPositionEyeVector = glm::vec3(0, 0, 0);
    mousePosition = glm::vec2(0, 0);

}

void Camera::UpdateView() {

    //roll can be removed from here. because is not actually used in FPS camera
    glm::mat4 matRoll = glm::mat4(1.0f);//identity matrix;
    glm::mat4 matPitch = glm::mat4(1.0f);//identity matrix
    glm::mat4 matYaw = glm::mat4(1.0f);//identity matrix

    //roll, pitch and yaw are used to store our angles in our class
    matRoll = glm::rotate(matRoll, roll, glm::vec3(0.0f, 0.0f, 1.0f));
    matPitch = glm::rotate(matPitch, pitch, glm::vec3(1.0f, 0.0f, 0.0f));
    matYaw = glm::rotate(matYaw, yaw, glm::vec3(0.0f, 1.0f, 0.0f));

    //order matters
    glm::mat4 rotationByMouse = matRoll * matPitch * matYaw;

//    glm::mat4 view = glm::mat4(1.0f);
    glm::mat4 view=glm::lookAt(
            glm::vec3(0.0f,0.05f,1.5f),
            glm::vec3(0.0f,0.0f,1.5f),
            glm::vec3(0.0f,0.0f,1.0f));

    view = glm::translate(view, -cameraPositionEyeVector);
    viewMatrix = rotationByMouse * view;
    //viewMatrix =  glm::rotate(viewMatrix, -glm::half_pi<float>(), glm::vec3(1,0,0));
    //LOG_EVERY_N(100, INFO) << "Eye: " << cameraPositionEyeVector.x << " " << cameraPositionEyeVector.y << " " << cameraPositionEyeVector.z;
}

glm::mat4 Camera::GetViewMatrix() const {
    return viewMatrix;
}

void Camera::MouseMove(float x, float y, float width, float height) {
    //always compute delta
    //mousePosition is the last mouse position
    glm::vec2 mouse_delta = glm::vec2(x, y) - mousePosition;

    const float mouseX_Sensitivity = 0.001f;
    const float mouseY_Sensitivity = 0.001f;
    //note that yaw and pitch must be converted to radians.
    //this is done in UpdateView() by glm::rotate
    yaw += mouseX_Sensitivity * mouse_delta.x;
    pitch += mouseY_Sensitivity * mouse_delta.y;

    mousePosition = glm::vec2(x, y);
    UpdateView();
}

void Camera::KeyPressed(const unsigned char key) {
    float dx = 0; //how much we strafe on x
    float dz = 0; //how much we walk on z
    float acc = 15;
    switch (key) {
        case 'w': {
            dz = 1 * acc;
            break;
        }

        case 's': {
            dz = -1 * acc;
            break;
        }
        case 'a': {
            dx = -1 * acc;
            break;
        }

        case 'd': {
            dx = 1 * acc;
            break;
        }
        default:
            break;
    }

    //get current view matrix
    glm::mat4 mat = GetViewMatrix();
    //mat = glm::rotate(mat, glm::half_pi<float>(), glm::vec3(1,0,0));
    //row major
    glm::vec3 forward(mat[0][2], mat[1][2], mat[2][2]);
    glm::vec3 strafe(mat[0][0], mat[1][0], mat[2][0]);

    const float speed = 0.12f;//how fast we move

    //forward vector must be negative to look forward.
    //read :http://in2gpu.com/2015/05/17/view-matrix/
    cameraPositionEyeVector += (-dz * forward + dx * strafe) * speed;

    //update the view matrix
    UpdateView();
}