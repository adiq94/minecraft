
#include <glm/detail/type_mat.hpp>
#include <Core/Model/Line.h>
#include <easylogging++.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

void Line::Draw(const glm::mat4 &projection_matrix, const glm::mat4 &view_matrix) {
    CheckError("before");
    glUseProgram(program);
    glUniformMatrix4fv(glGetUniformLocation(program, "view_matrix"), 1, GL_FALSE, glm::value_ptr(view_matrix));
    CheckError("after viewMatrix");
    glUniformMatrix4fv(glGetUniformLocation(program, "projection_matrix"), 1, GL_FALSE, glm::value_ptr(projection_matrix));
    CheckError("after projectionMatrix");
    glBindVertexArray(vao);
    drawAction(this);
    glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(translatedVertices), glm::value_ptr(translatedVertices), GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colors), glm::value_ptr(colors), GL_DYNAMIC_DRAW);

    glDrawArrays(GL_LINES, 0, 2);
}

void Line::Create(std::function<void(Line* line)> createAction, std::function<void(Line* line)> drawAction) {
    createAction(this);
    this->drawAction = drawAction;

    GLuint vbo[2];
    glGenBuffers(2, vbo);
    vbos.push_back(vbo[0]);
    vbos.push_back(vbo[1]);
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), glm::value_ptr(vertices), GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colors), glm::value_ptr(colors), GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glBindVertexArray(0);

}
