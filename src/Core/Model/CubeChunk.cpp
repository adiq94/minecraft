#include <Core/Model/CubeChunk.h>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <Core/Texture/Texture.h>
#include "Core/Manager/CubeTextureManager.h"


CubeChunk::CubeChunk()
{
}


CubeChunk::~CubeChunk()
{
}
void CubeChunk::Create(int length, int width, int height, glm::vec3 offset)
{

    this->length = length;
    this->width = width;
    this->height = height;
    GLuint vao;
    GLuint vbo;
    /*
     *    6---7
         /|  /|
        2---3 |
        | 4-|-5
        |/  |/
        0---1

        0 - 0, 0, 0,
        1 - 1, 0, 0,
        2 - 0, 1, 0,
        3 - 1, 1, 0,
        4 - 0, 0, 1,
        5 - 1, 0, 1,
        6 - 0, 1, 1,
        7 - 1, 1, 1,

        Sides:
        2---3  3---7  7---6  6---2
        |   |  |   |  |   |  |   |
        |   |  |   |  |   |  |   |
        0---1  1---5  5---4  4---0

        Bottom/Top
        0---1  6---7
        |   |  |   |
        |   |  |   |
        4---5  2---3

        2---3                3         2---3
        |   |  becomes      /|   and   |  /
        |   |             /  |         |/
        0---1            0---1         0

        Triangles 0-1-3 and 0-3-2
        Counter clockwise direction
     *
     */
    float vertices[]{
            //BOTTOM
            /*
             *  2---3
                |   |
                |   |
                0---1
             */
            0, 0, 0,
            1, 0, 0,
            1, 1, 0,

            0, 0, 0,
            1, 1, 0,
            0, 1, 0,

            /*
             *  3---7
                |   |
                |   |
                1---5
             */

            1, 0, 0,
            1, 0, 1,
            1, 1, 1,

            1, 0, 0,
            1, 1, 1,
            1, 1, 0,

            /*
             *  7---6 TOP
             *  |   |
             *  |   |
             *  5---4
             */

            1, 0, 1,
            0, 0, 1,
            0, 1, 1,


            1, 0, 1,
            0, 1, 1,
            1, 1, 1,

            /*
             *  6---2
                |   |
                |   |
                4---0
             */

            0, 0, 1,
            0, 0, 0,
            0, 1, 0,

            0, 0, 1,
            0, 1, 0,
            0, 1, 1,

            /*
             *  0---1
                |   |
                |   |
                4---5
             */

            0, 0, 1,
            1, 0, 1,
            1, 0, 0,

            0, 0, 1,
            1, 0, 0,
            0, 0, 0,

            /*
             *  6---7
                |   |
                |   |
                2---3
             */

            0, 1, 0,
            1, 1, 0,
            1, 1, 1,

            0, 1, 0,
            1, 1, 1,
            0, 1, 1,
    };

    Texture t = Texture("resources/Textures/testAtlas2.png");
    t.bind();
    CubeTextureManager cubeTexManager = CubeTextureManager();
    auto textureCoords = cubeTexManager.GetTextureCoordsFromSource(64, {AtlasIndex(0, 0),
                                                                        AtlasIndex(0, 1),
                                                                        AtlasIndex(1, 0),
                                                                        AtlasIndex(1, 1),
                                                                        AtlasIndex(1, 0),
                                                                        AtlasIndex(1, 0),});

    // Generate a list of 100 quad locations/translation-vectors
    std::vector<glm::vec3> translations;
    int index = 0;
    GLfloat offsetInstance = 1.0f;
    for(GLint x = 0; x < length; x++)
    {
        for(GLint y = 0; y < width; y++)
        {
            for(GLint z = 0; z < height; z++) {
                glm::vec3 translation;
                translation.x = (GLfloat) (offset.x + x * offsetInstance);
                translation.y = (GLfloat) (offset.y + y * offsetInstance);
                translation.z = (GLfloat) (offset.z + z * offsetInstance);
                translations.push_back(translation);
            }
        }
    }

    // Store instance data in an array buffer
    GLuint instanceVBO;
    glGenBuffers(1, &instanceVBO);
    glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * length*width*height, &translations[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    GLuint offsetBuffer = 0;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices) + textureCoords.size() * sizeof(GLfloat), NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, offsetBuffer, sizeof(vertices), vertices);
    offsetBuffer += sizeof(vertices);
    glBufferSubData(GL_ARRAY_BUFFER, offsetBuffer, textureCoords.size() * sizeof(GLfloat), textureCoords.data());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)sizeof(vertices));

    glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glVertexAttribDivisor(2, 1);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glBindVertexArray(0);
    this->vao = vao;
    this->vbos.push_back(vbo);

}

void CubeChunk::Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix)
{
    CheckError("before");
    glUseProgram(program);
    glUniformMatrix4fv(glGetUniformLocation(program, "view_matrix"), 1, GL_FALSE, glm::value_ptr(view_matrix));
    CheckError("after viewMatrix");
    glUniformMatrix4fv(glGetUniformLocation(program, "projection_matrix"), 1, GL_FALSE, glm::value_ptr(projection_matrix));
    CheckError("after projectionMatrix");
    glBindVertexArray(vao);
    glDrawArraysInstanced(GL_TRIANGLES, 0, 36, length*width*height);
}

void CubeChunk::CheckError(std::string name) {
    auto er = glGetError();
    if(er != GL_NO_ERROR){
        std::cout << name << ": " << er << std::endl;
    }
}
