
#include <glm/detail/type_mat.hpp>
#include <Core/Model/Model.h>
#include <Core/Model/Cube.h>
#include <Core/Manager/CubeTextureManager.h>
#include <Core/Texture/Texture.h>
#include <glm/gtc/type_ptr.hpp>
#include <easylogging++.h>
#include <Core/Manager/PhysicsManager.h>

void Cube::Draw(const glm::mat4 &projection_matrix, const glm::mat4 &view_matrix) {
    CheckError("before");
    glUseProgram(program);
    glUniformMatrix4fv(glGetUniformLocation(program, "view_matrix"), 1, GL_FALSE, glm::value_ptr(view_matrix));
    CheckError("after viewMatrix");
    glUniformMatrix4fv(glGetUniformLocation(program, "projection_matrix"), 1, GL_FALSE, glm::value_ptr(projection_matrix));
    CheckError("after projectionMatrix");
    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbos[2]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(position), glm::value_ptr(position), GL_DYNAMIC_DRAW);

    glDrawArrays(GL_TRIANGLES, 0, 36);
}

void Cube::Create(btRigidBody *body) {
    this->body = body;
    float vertices[]{
            //BOTTOM
            /*
             *  2---3
                |   |
                |   |
                0---1
             */
            0, 0, 0,
            1, 0, 0,
            1, 1, 0,

            0, 0, 0,
            1, 1, 0,
            0, 1, 0,

            /*
             *  3---7
                |   |
                |   |
                1---5
             */

            1, 0, 0,
            1, 0, 1,
            1, 1, 1,

            1, 0, 0,
            1, 1, 1,
            1, 1, 0,

            /*
             *  7---6 TOP
             *  |   |
             *  |   |
             *  5---4
             */

            1, 0, 1,
            0, 0, 1,
            0, 1, 1,


            1, 0, 1,
            0, 1, 1,
            1, 1, 1,

            /*
             *  6---2
                |   |
                |   |
                4---0
             */

            0, 0, 1,
            0, 0, 0,
            0, 1, 0,

            0, 0, 1,
            0, 1, 0,
            0, 1, 1,

            /*
             *  0---1
                |   |
                |   |
                4---5
             */

            0, 0, 1,
            1, 0, 1,
            1, 0, 0,

            0, 0, 1,
            1, 0, 0,
            0, 0, 0,

            /*
             *  6---7
                |   |
                |   |
                2---3
             */

            0, 1, 0,
            1, 1, 0,
            1, 1, 1,

            0, 1, 0,
            1, 1, 1,
            0, 1, 1,
    };

    Texture t = Texture("resources/Textures/testAtlas2.png");
    t.bind();
    CubeTextureManager cubeTexManager = CubeTextureManager();
    auto textureCoords = cubeTexManager.GetTextureCoordsFromSource(64, {AtlasIndex(0, 0),
                                                                        AtlasIndex(0, 1),
                                                                        AtlasIndex(1, 0),
                                                                        AtlasIndex(1, 1),
                                                                        AtlasIndex(1, 0),
                                                                        AtlasIndex(1, 0),});

    GLuint vbo[3];
    glGenBuffers(3, vbo);
    vbos.push_back(vbo[0]);
    vbos.push_back(vbo[1]);
    vbos.push_back(vbo[2]);
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
    glBufferData(GL_ARRAY_BUFFER, textureCoords.size() * sizeof(GLfloat), textureCoords.data(), GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, vbos[2]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(position), glm::value_ptr(position), GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
    glBindBuffer(GL_ARRAY_BUFFER, vbos[2]);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    glVertexAttribDivisor(2, 1);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    glBindVertexArray(0);
}

void Cube::CheckError(std::string name) {
    auto er = glGetError();
    if(er != GL_NO_ERROR){
        LOG(INFO) << name << ": " << er;
    }
}

Cube::Cube() {}

Cube::~Cube() {

}

void Cube::Update(std::map<btRigidBody *, glm::vec3> map) {
    position = map[body];
    position.x = position.x - 0.5;
    position.y = position.y - 0.5;
    position.z = position.z - 0.5;
    //LOG_EVERY_N(100, INFO) << "x: " << position.x << " y: " << position.y << " z: " << position.z;
}
