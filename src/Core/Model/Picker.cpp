//
// Created by Albert on 16.04.2017.
//

#include <Core/Utility/MousePicker.h>
#include "Core/Model/Picker.h"

void Picker::Draw(const glm::mat4 &projection_matrix, const glm::mat4 &view_matrix) {
    MousePicker *picker = new MousePicker(projection_matrix, view_matrix, 1280, 720);
    picker->update();
}
