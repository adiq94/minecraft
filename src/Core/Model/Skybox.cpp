
#include <glm/detail/type_mat.hpp>
#include <Core/Model/Skybox.h>
#include <Core/Texture/Cubemap.h>
#include <iostream>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

GLfloat skyboxVertices[] = {
        // Positions
        -1.0f, 1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,

        -1.0f, -1.0f, 1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,

        1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,

        -1.0f, -1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,

        -1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, -1.0f,

        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f
};

void Skybox::Draw(const glm::mat4 &projection_matrix, const glm::mat4 &view_matrix) {
    glDepthMask(GL_FALSE);
    glFrontFace(GL_CW);

    CheckError("before skybox");
    glUseProgram(program);
    glm::mat4 view = glm::mat4(glm::mat3(view_matrix));
    view = glm::rotate(view, glm::half_pi<float>(), glm::vec3(1, 0,0));
    glUniformMatrix4fv(glGetUniformLocation(program, "view"), 1, GL_FALSE, glm::value_ptr(view));
    CheckError("after viewMatrix skybox");
    glUniformMatrix4fv(glGetUniformLocation(program, "projection"), 1, GL_FALSE, glm::value_ptr(projection_matrix));
    CheckError("after projectionMatrix skybox");

    glBindVertexArray(vao);
    skybox->Bind();
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);

    glFrontFace(GL_CCW);
    glDepthMask(GL_TRUE);


}

void Skybox::Create() {
    CheckError("before vao");
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &skyboxVBO);
    CheckError("after vbo");
    skybox = new Cubemap(GetTextureLocation("desertsky_rt"), GetTextureLocation("desertsky_lf"),
                           GetTextureLocation("desertsky_up"), GetTextureLocation("desertsky_dn"),
                           GetTextureLocation("desertsky_bk"), GetTextureLocation("desertsky_ft"));

    glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
    CheckError("after bind buffer");
    glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
    CheckError("after buffer");
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    CheckError("after glVertexAttribPointer");
    glEnableVertexAttribArray(0);
    CheckError("after enableVertexAttrib");
    glBindVertexArray(0);
}

std::string Skybox::GetTextureLocation(std::string name) {
    std::string baseDir = "resources/Textures/Skybox/";
    baseDir += name;
    baseDir += ".tga";
    return baseDir;
};

void Skybox::CheckError(std::string name) {
    auto er = glGetError();
    if(er != GL_NO_ERROR){
        std::cout << name << ": " << er << std::endl;
    }
}

