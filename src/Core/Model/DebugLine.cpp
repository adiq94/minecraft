#include <Core/Model/DebugLine.h>
#include <glm/gtc/type_ptr.hpp>

void DebugLine::Create(std::vector<float> vertices, std::vector<float> colors) {
    this->vertices = vertices;
    this->colors = colors;

    GLuint vbo[2];
    glGenBuffers(2, vbo);
    vbos.push_back(vbo[0]);
    vbos.push_back(vbo[1]);
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), vertices.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
    glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(GLfloat), colors.data(), GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glBindVertexArray(0);
}

void DebugLine::Draw(const glm::mat4 &projection_matrix, const glm::mat4 &view_matrix) {
    CheckError("before");
    glUseProgram(program);
    glUniformMatrix4fv(glGetUniformLocation(program, "view_matrix"), 1, GL_FALSE, glm::value_ptr(view_matrix));
    CheckError("after viewMatrix");
    glUniformMatrix4fv(glGetUniformLocation(program, "projection_matrix"), 1, GL_FALSE, glm::value_ptr(projection_matrix));
    CheckError("after projectionMatrix");
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), vertices.data(), GL_STATIC_DRAW);
    CheckError("after vbos[0]");
    glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
    glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(GLfloat), colors.data(), GL_STATIC_DRAW);
    CheckError("after vbos[1]");
    glDrawArrays(GL_LINES, 0, vertices.size() / 3);
    CheckError("after glDrawArrays");
}
