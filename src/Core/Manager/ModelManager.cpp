#include <Core/Manager/ModelManager.h>

ModelManager::ModelManager()
{
}

ModelManager::~ModelManager()
{

    for (auto it = gameModelList.begin(); it != gameModelList.end(); it++)
    {
        delete it->second;
    }
    gameModelList.clear();

    for (auto it = gameModelList_NDC.begin(); it != gameModelList_NDC.end(); it++)
    {
        delete it->second;
    }
    gameModelList_NDC.clear();
}

//NDC
void ModelManager::Draw()
{
    for (auto it = gameModelList_NDC.begin(); it != gameModelList_NDC.end(); it++)
    {
        it->second->Draw();
    }
}

void ModelManager::Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix)
{
    for (auto it = gameModelList.begin(); it != gameModelList.end(); it++)
    {
        it->second->Draw(projection_matrix, view_matrix);
    }
}

void ModelManager::DeleteModel(const std::string& gameModelName)
{

    IEntity* model = gameModelList[gameModelName];
    model->Destroy();
    gameModelList.erase(gameModelName);

}

void ModelManager::DeleteModel_NDC(const std::string& gameModelName)
{

    IEntity* model = gameModelList_NDC[gameModelName];
    model->Destroy();
    gameModelList_NDC.erase(gameModelName);

}

const IEntity& ModelManager::GetModel(const std::string& gameModelName) const
{
    return (*gameModelList.at(gameModelName));
}

const IEntity& ModelManager::GetModel_NDC(const std::string& gameModelName) const
{
    return (*gameModelList_NDC.at(gameModelName));
}

void ModelManager::SetModel(const std::string& gameObjectName, IEntity* gameObject)
{
    gameModelList[gameObjectName.c_str()] = gameObject;
}

void ModelManager::Update(std::map<btRigidBody *, glm::vec3> map) {
    for (auto it = gameModelList.begin(); it != gameModelList.end(); it++)
    {
        it->second->Update(map);
    }
    for (auto it = gameModelList_NDC.begin(); it != gameModelList_NDC.end(); it++)
    {
        it->second->Update(map);
    }
}
