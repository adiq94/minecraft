//
// Created by Albert on 15.04.2017.
//

#include "Core/Manager/CubeTextureManager.h"

std::vector<float> CubeTextureManager::GetTextureCoordsFromSource(int size, std::vector<AtlasIndex> indexes) {
    
    auto coordinates = std::vector<float>();
    float itemSize = size / itemCount;

    for( auto &item : indexes ){

        float vertexLeftTopX = itemSize/size * item.x;
        float vertexLeftTopY = 1 - (itemSize/size) * item.y;

        float vertexRightTopX = itemSize/size * (item.x + 1);
        float vertexRightTopY = 1 - (itemSize/size) * item.y;

        float vertexLeftBottomX = itemSize/size * item.x;
        float vertexLeftBottomY = 1 - (itemSize/size) * (item.y + 1);

        float vertexRightBottomX = itemSize/size * (item.x + 1);
        float vertexRightBottomY = 1 - (itemSize/size) * (item.y + 1);

        // first triangle
        coordinates.push_back(vertexLeftBottomX);
        coordinates.push_back(vertexLeftBottomY);
        coordinates.push_back(vertexRightBottomX);
        coordinates.push_back(vertexRightBottomY);
        coordinates.push_back(vertexRightTopX);
        coordinates.push_back(vertexRightTopY);

        // second triangle
        coordinates.push_back(vertexLeftBottomX);
        coordinates.push_back(vertexLeftBottomY);
        coordinates.push_back(vertexRightTopX);
        coordinates.push_back(vertexRightTopY);
        coordinates.push_back(vertexLeftTopX);
        coordinates.push_back(vertexLeftTopY);

    }

    
    return coordinates;
}

AtlasIndex::AtlasIndex(int x, int y) : x(x), y(y) {}
