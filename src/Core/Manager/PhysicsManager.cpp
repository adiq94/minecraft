
#include <Core/Manager/PhysicsManager.h>
#include <BulletCollision/BroadphaseCollision/btDbvtBroadphase.h>
#include <BulletCollision/CollisionShapes/btBoxShape.h>
#include <LinearMath/btDefaultMotionState.h>
#include <GLFW/glfw3.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include <BulletCollision/CollisionShapes/btCapsuleShape.h>
#include <BulletDynamics/Character/btCharacterControllerInterface.h>
#include <LinearMath/btIDebugDraw.h>
#include <Core/Utility/DebugDrawer.h>
#include <yaml-cpp/node/node.h>
#include <yaml-cpp/yaml.h>

void PhysicsManager::Init(ModelManager *manager) {
    collisionConfiguration = new btDefaultCollisionConfiguration();

    ///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
    dispatcher = new btCollisionDispatcher(collisionConfiguration);

    ///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
    overlappingPairCache = new btDbvtBroadphase();
    overlappingPairCache->getOverlappingPairCache()->setInternalGhostPairCallback(new btGhostPairCallback);

    ///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
    solver = new btSequentialImpulseConstraintSolver;

    dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);

    dynamicsWorld->setGravity(btVector3(0, 0, -10));

    btCollisionShape *groundShape = new btBoxShape(btVector3(btScalar(8), btScalar(8), btScalar(8)));
    collisionShapes.push_back(groundShape);

    btTransform groundTransform;
    groundTransform.setIdentity();
    groundTransform.setOrigin(btVector3(8, 8, 8));

    btScalar mass(0.);
    bool isDynamic = (mass != 0.);

    btVector3 localInertia(0, 0, 0);
    if (isDynamic)
        groundShape->calculateLocalInertia(mass, localInertia);

    //using motionstate is optional, it provides interpolation capabilities, and only synchronizes 'active' objects
    btDefaultMotionState *myMotionState = new btDefaultMotionState(groundTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, groundShape, localInertia);
    btRigidBody *body = new btRigidBody(rbInfo);

    //add the body to the dynamics world
    dynamicsWorld->addRigidBody(body);
    YAML::Node config = YAML::LoadFile("config.yaml");
    auto physicsDebug = config["physics_debug"].as<bool>();
    if(physicsDebug) {
        drawer = new DebugDrawer(manager);
        dynamicsWorld->setDebugDrawer(drawer);
        dynamicsWorld->getDebugDrawer()->setDebugMode(btIDebugDraw::DBG_DrawWireframe);
    }


}

void PhysicsManager::Update(float frameRate) {
    if(frameRate > 5 && glfwGetTime() > 2) {
        frameRate = std::max(frameRate, 60.f);
        auto timeStep = 1.f / frameRate;
        auto maxSubSteps = 2;
        auto numSimSteps = dynamicsWorld->stepSimulation(timeStep, maxSubSteps);
        if (numSimSteps > maxSubSteps)
        {
            //detect dropping frames
            printf("Dropped (%i) simulation steps out of %i\n",numSimSteps - maxSubSteps,numSimSteps);
        }

        //print positions of all objects
        for (int j = dynamicsWorld->getNumCollisionObjects() - 1; j >= 0; j--) {
            btCollisionObject *obj = dynamicsWorld->getCollisionObjectArray()[j];
            btRigidBody *body = btRigidBody::upcast(obj);
            btTransform trans;
            if (body && body->getMotionState()) {
                body->getMotionState()->getWorldTransform(trans);

            } else {
                trans = obj->getWorldTransform();
            }
            PhysicsPositions[body] = glm::vec3(trans.getOrigin().getX(), trans.getOrigin().getY(),
                                               trans.getOrigin().getZ());
        }
    }
}

PhysicsManager::~PhysicsManager() {
    for (int i = dynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i--) {
        btCollisionObject *obj = dynamicsWorld->getCollisionObjectArray()[i];
        btRigidBody *body = btRigidBody::upcast(obj);
        if (body && body->getMotionState()) {
            delete body->getMotionState();
        }
        dynamicsWorld->removeCollisionObject(obj);
        delete obj;
    }

    //delete collision shapes
    for (int j = 0; j < collisionShapes.size(); j++) {
        btCollisionShape *shape = collisionShapes[j];
        collisionShapes[j] = 0;
        delete shape;
    }

    //delete dynamics world
    delete dynamicsWorld;

    //delete solver
    delete solver;

    //delete broadphase
    delete overlappingPairCache;

    //delete dispatcher
    delete dispatcher;

    delete collisionConfiguration;

    //next line is optional: it will be cleared by the destructor when the array goes out of scope
    collisionShapes.clear();
}

btRigidBody *PhysicsManager::CreateCube(glm::vec3 position) {
//create a dynamic rigidbody

    btCollisionShape* colShape = new btBoxShape(btVector3(0.5,0.5,0.5));
    //btCollisionShape* colShape = new btSphereShape(btScalar(1.));
    collisionShapes.push_back(colShape);

    /// Create Dynamic Objects
    btTransform startTransform;
    startTransform.setIdentity();

    btScalar mass(1.f);

    //rigidbody is dynamic if and only if mass is non zero, otherwise static
    bool isDynamic = (mass != 0.f);

    btVector3 localInertia(0,0,0);
    if (isDynamic)
        colShape->calculateLocalInertia(mass,localInertia);
    startTransform.setOrigin(btVector3(position.x,position.y,position.z));

    //using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
    btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass,myMotionState,colShape,localInertia);
    btRigidBody* body = new btRigidBody(rbInfo);

    dynamicsWorld->addRigidBody(body);
    PhysicsPositions[body] = glm::vec3(position.x,position.y,position.z);
    return body;
}

std::map<btRigidBody *, glm::vec3> PhysicsManager::GetMap() {
    return PhysicsPositions;
}

///playerStepCallback is the main function that is updating the character.
///Register this callback using: m_dynamicsWorld->setInternalTickCallback(playerStepCallback,m_character);
///This function will be called at the end of each internal simulation time step
void playerStepCallback(btDynamicsWorld* dynamicsWorld, btScalar timeStep)
{
    btCharacterControllerInterface* characterInterface= (btCharacterControllerInterface*) dynamicsWorld->getWorldUserInfo();
    characterInterface->preStep (dynamicsWorld);
    characterInterface->playerStep (dynamicsWorld, timeStep);

}

void PhysicsManager::AddPlayer() {
    ghost = new btPairCachingGhostObject();

    btTransform startTransform;
    startTransform.setIdentity();
    startTransform.setOrigin (btVector3(5, 5, 150));
    startTransform.setRotation(btQuaternion(btVector3(1,0,0), btScalar(3.14/2)));

    ghost->setWorldTransform(startTransform);
    btConvexShape *shape = new btCapsuleShape(0.5,2);
    ghost->setCollisionShape(shape);
    ghost->setCollisionFlags(btCollisionObject::CF_CHARACTER_OBJECT);
    playerController = new btKinematicCharacterController(ghost, shape, 0.35);
    playerController->setGravity(btVector3(0,0,-10));

    dynamicsWorld->addCollisionObject(ghost, btBroadphaseProxy::CharacterFilter, btBroadphaseProxy::StaticFilter|btBroadphaseProxy::DefaultFilter);
    dynamicsWorld->addAction(playerController);

    btTransform xform;
    xform = ghost->getWorldTransform();

    btVector3 forwardDir = xform.getBasis()[2];
    //	printf("forwardDir=%f,%f,%f\n",forwardDir[0],forwardDir[1],forwardDir[2]);
    btVector3 upDir = xform.getBasis()[1];
    btVector3 strafeDir = xform.getBasis()[0];
    forwardDir.normalize ();
    upDir.normalize ();
    strafeDir.normalize ();

    btVector3 walkDirection = btVector3(0.0, 0.0, 0.0);

    playerController->setWalkDirection(walkDirection * 0);

    dynamicsWorld->setInternalTickCallback(playerStepCallback,playerController);
}

btKinematicCharacterController *PhysicsManager::GetPlayerController() {
    return playerController;
}

btPairCachingGhostObject *PhysicsManager::GetGhost() {
    return ghost;
}

btDiscreteDynamicsWorld *PhysicsManager::GetWorld() {
    return dynamicsWorld;
}

DebugDrawer *PhysicsManager::GetDebugDrawer() {
    return drawer;
}


