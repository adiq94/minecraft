#include <Core/Manager/SceneManager.h>
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>
#include<glm/gtc/constants.hpp>
#include <yaml-cpp/node/node.h>
#include <yaml-cpp/yaml.h>
#include "Core/Utility/MousePicker.h"
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include <easylogging++.h>


const float PI = 3.141592653589793f;

SceneManager::SceneManager(float width,float height) {
    widthForCamera = width;
    heightForCamera = height;
    glEnable(GL_DEPTH_TEST);

    shader_manager = new ShaderManager();
    shader_manager->CreateProgram("colorShader",
                                  "resources/Shaders/SimpleVertex.glsl",
                                  "resources/Shaders/SimpleFragment.glsl");
    shader_manager->CreateProgram("skyboxShader", "resources/Shaders/SkyboxVertex.glsl",
                                  "resources/Shaders/SkyboxFragment.glsl");
    shader_manager->CreateProgram("lineShader", "resources/Shaders/LineVertex.glsl", "resources/Shaders/LineFragment.glsl");
    physics_manager = new PhysicsManager();
    models_manager = new ModelManager();
    physics_manager->Init(models_manager);
    physics_manager->AddPlayer();

    float ar = width / height;
    float angle = glm::quarter_pi<float>(), near1 = 0.1f, far1 = 2000.0f;

    projection_matrix = glm::perspective<float>(angle, ar, near1, far1);
    camera = new Camera();


    camera->UpdateView();
    view_matrix = camera->GetViewMatrix();
}

SceneManager::~SceneManager() {

    delete shader_manager;
    delete models_manager;
    physics_manager->~PhysicsManager();
}

void SceneManager::NotifyBeginFrame(float framerate) {
    YAML::Node config = YAML::LoadFile("config.yaml");
    auto fpsCamera = config["fps_camera"].as<bool>();
    if(fpsCamera) {
        auto ghost = physics_manager->GetGhost();
        auto position = ghost->getWorldTransform().getOrigin();
        auto x = position.getX();
        auto y = position.getY();
        auto z = position.getZ();
       //LOG_EVERY_N(60, INFO) << x << " " << y << " " << z;
        camera->cameraPositionEyeVector = glm::vec3(x,y,z);
    }
    physics_manager->Update(framerate);
    models_manager->Update(physics_manager->GetMap());
}

void SceneManager::NotifyDisplayFrame() {

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.0, 0.0, 0.0, 1.0);

    models_manager->Draw();
    models_manager->Draw(projection_matrix, view_matrix);
    camera->UpdateView();
    view_matrix = camera->GetViewMatrix();
}

void SceneManager::NotifyEndFrame() {
    YAML::Node config = YAML::LoadFile("config.yaml");
    auto physicsDebug = config["physics_debug"].as<bool>();
    if(physicsDebug) {
        physics_manager->GetWorld()->debugDrawWorld();
        physics_manager->GetDebugDrawer()->update();
    }
}

void SceneManager::NotifyReshape(int width, int height,
                                 int previos_width, int previous_height) {
}

ModelManager *SceneManager::GetModels_Manager() {
    return models_manager;
}

int MoveCoordinates[2] = {0,0}; // --LEFT - RIGHT++, --DOWN - UP++

void SceneManager::KeyboardCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
    YAML::Node config = YAML::LoadFile("config.yaml");
    auto fpsCamera = config["fps_camera"].as<bool>();
    if(!fpsCamera) {
        if (key == GLFW_KEY_A) {
            camera->KeyPressed('a');
        }
        if (key == GLFW_KEY_D) {
            camera->KeyPressed('d');
        }
        if (key == GLFW_KEY_W) {
            camera->KeyPressed('w');
        }
        if (key == GLFW_KEY_S) {
            camera->KeyPressed('s');
        }
    } else{
        auto controller = physics_manager->GetPlayerController();
        auto ghost = physics_manager->GetGhost();

        if (key == GLFW_KEY_A && action == GLFW_PRESS) {
            MoveCoordinates[0]--;
        }
        if (key == GLFW_KEY_A && action == GLFW_RELEASE) {
            MoveCoordinates[0]++;
        }
        if (key == GLFW_KEY_D && action == GLFW_PRESS) {
            MoveCoordinates[0]++;
        }
        if (key == GLFW_KEY_D && action == GLFW_RELEASE) {
            MoveCoordinates[0]--;
        }
        if (key == GLFW_KEY_W && action == GLFW_PRESS) {
            MoveCoordinates[1]++;
        }
        if (key == GLFW_KEY_W && action == GLFW_RELEASE) {
            MoveCoordinates[1]--;
        }
        if (key == GLFW_KEY_S && action == GLFW_PRESS) {
            MoveCoordinates[1]--;
        }
        if (key == GLFW_KEY_S && action == GLFW_RELEASE) {
            MoveCoordinates[1]++;
        }
        if (key == GLFW_KEY_SPACE){
            if(controller->canJump()) {
                controller->jump(btVector3(0,0,10));
            }
        }
        SetWalkDirection(controller, ghost);
    }
}

//int GetAngle01_00_XY(int x, int y)
//{
//    POINTFLOAT ab = { 0 - 0, 0 - 1 };
//    POINTFLOAT cb = { 0 - x, 0 - y };
//
//    // dot product
//    float dot = (ab.x * cb.x + ab.y * cb.y);
//
//    // length square of both vectors
//    float abSqr = ab.x * ab.x + ab.y * ab.y;
//    float cbSqr = cb.x * cb.x + cb.y * cb.y;
//
//    // square of cosine of the needed angle
//    float cosSqr = dot * dot / abSqr / cbSqr;
//
//    // this is a known trigonometric equality:
//    // cos(alpha * 2) = [ cos(alpha) ]^2 * 2 - 1
//    float cos2 = 2 * cosSqr - 1;
//
//    // Here's the only invocation of the heavy function.
//    // It's a good idea to check explicitly if cos2 is within [-1 .. 1] range
//
//    const float pi = 3.141592f;
//
//    float alpha2 =
//            (cos2 <= -1) ? pi :
//            (cos2 >= 1) ? 0 :
//            acosf(cos2);
//
//    float rslt = alpha2 / 2;
//
//    float rs = rslt * 180. / pi;
//
//
//    // Now revolve the ambiguities.
//    // 1. If dot product of two vectors is negative - the angle is definitely
//    // above 90 degrees. Still we have no information regarding the sign of the angle.
//
//    // NOTE: This ambiguity is the consequence of our method: calculating the cosine
//    // of the double angle. This allows us to get rid of calling sqrt.
//
//    if (dot < 0)
//        rs = 180 - rs;
//
//    // 2. Determine the sign. For this we'll use the Determinant of two vectors.
//
//    float det = (ab.x * cb.y - ab.y * cb.y);
//    if (det < 0)
//        rs = -rs;
//
//    return (int) floor(rs + 0.5);
//}


void SceneManager::SetWalkDirection(btKinematicCharacterController *controller, btPairCachingGhostObject *ghost) {
    btTransform xform;
    xform = ghost->getWorldTransform();

    btVector3 forwardDir = xform.getBasis()[2];
    btVector3 upDir = xform.getBasis()[1];
    btVector3 strafeDir = xform.getBasis()[0];
    forwardDir.normalize ();
    upDir.normalize ();
    strafeDir.normalize ();

    glm::mat4 dsa = camera->GetViewMatrix();
    glm::vec3 lookAt = camera->cameraPositionEyeVector;
   // glm::normalize(dsa);

    //printf("forwardDir=%f,%f,%f\n",forwardDir.getX(),forwardDir.getY(),forwardDir.getZ());
    printf("forwardDir=%f,%f,%f\n",lookAt[0],lookAt[1],lookAt[2]);
    printf("VM0=%f,%f,%f,%f\n",dsa[0][0],dsa[0][1],dsa[0][2],dsa[0][3]);
    printf("VM1=%f,%f,%f,%f\n",dsa[1][0],dsa[1][1],dsa[1][2],dsa[1][3]);
    printf("VM2=%f,%f,%f,%f\n",dsa[2][0],dsa[2][1],dsa[2][2],dsa[2][3]);
            printf("VM3=%f,%f,%f,%f\n",dsa[3][0],dsa[3][1],dsa[3][2],dsa[3][3]);

            btVector3 dirVector = {
//            forwardDir.getX() + MoveCoordinates[0]
//            , forwardDir.getY() + MoveCoordinates[1]
//            int(dsa[0][0]) * MoveCoordinates[0]
//            , int(dsa[0][1]) * MoveCoordinates[1]
                //dsa[0][0] * MoveCoordinates[1]
                //,dsa[1][0] * MoveCoordinates[1]
                //, 0
            dsa[0][1]
            , dsa[1][1]
            , 0
    };

    //glm::normalize(dirVector);

    dirVector.normalize();

    //dirVector.rotate({0,1,0}, GetAngle01_00_XY(MoveCoordinates[0], MoveCoordinates[1]));
    if (MoveCoordinates[1] == 1){
        //nothign
    }
    if (MoveCoordinates[1] == 0){
        if (MoveCoordinates[0] == 1){
            dirVector = dirVector.rotate({0,0,1}, (PI / 2) * (-1));
        }
        if (MoveCoordinates[0] == -1){
            dirVector = dirVector.rotate({0,0,1}, PI / 2);
        }
        if (MoveCoordinates[0] == 0){
            dirVector = {0,0,0};
        }
    }
    if (MoveCoordinates[1] == -1){
        dirVector = {
                dirVector.getX() * (-1)
                , dirVector.getY() * (-1)
                , 0
        };
    }

    btVector3 walkDirection = dirVector;
    btScalar walkVelocity = btScalar(1.1) * 4.0; // 4 km/h -> 1.1 m/s
    btScalar walkSpeed = walkVelocity * 0.005;

    controller->setWalkDirection(walkDirection * walkSpeed);
}

void SceneManager::cursor_position_callback(GLFWwindow *window, double xpos, double ypos) {
    camera->MouseMove(xpos, ypos, widthForCamera, heightForCamera);
    view_matrix = camera->GetViewMatrix();
}

void SceneManager::scroll_callback(GLFWwindow *window, double xoffset, double yoffset) {
    //std::cout << "x:" << xoffset << std::endl << "y:" << yoffset << std::endl;
    view_matrix = glm::translate<float>(view_matrix, glm::vec3(0, 0, -yoffset));
}
PhysicsManager* SceneManager::GetPhysicsManager(){
    return physics_manager;
}

