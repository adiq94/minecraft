#include <Core/Manager/ShaderManager.h>
#include <iostream>
#include <fstream>
#include <vector>

std::map<std::string, GLuint> ShaderManager::programs;

ShaderManager::ShaderManager(void){}

ShaderManager::~ShaderManager(void)
{
    /**
     * ShaderManager class destructor.
     *
     * For every program in programs map performs glDeleteProgram
     * to delete shading program from memory.
     */
    for (auto const& x : programs)
    {
        glDeleteProgram(x.second);
    }
    programs.clear();
}

std::string ShaderManager::ReadShader(const std::string& filename)
{
    /**
     * Reads source code from .glsl files.
     * Returns string which contains source code.
     *
     * @param filename contains path and filename of .glsl file.
     * @return shaderCode
     */

    std::string shaderCode;
    std::ifstream file(filename, std::ios::in);

    if (!file.good()){
        std::cout << "Can't read file " << filename.c_str() << std::endl;
        std::terminate();
    }

    file.seekg(0, std::ios::end);
    shaderCode.resize((unsigned int)file.tellg());
    file.seekg(0, std::ios::beg);
    file.read(&shaderCode[0], shaderCode.size());
    file.close();

    return shaderCode;
}

GLuint ShaderManager::CreateShader(GLenum shaderType, const std::string& source, const std::string& shaderName)
{
    /**
     * Compiles .glsl code and returns shader.
     *
     * @param shaderType indicates shader type - GL_VERTEX_SHADER or GL_FRAGMENT_SHADER
     * @param source .glsl source code, output of ReadShader function
     * @param shaderName name of shader, used to help debugging
     * @return shader
     */

    int compile_result = 0;

    GLuint shader = glCreateShader(shaderType);
    const char *shader_code_ptr = source.c_str();
    const int   shader_code_size = source.size();

    glShaderSource(shader, 1, &shader_code_ptr, &shader_code_size);
    glCompileShader(shader);
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compile_result);

    if (compile_result == GL_FALSE)
    {

        int info_log_length = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_log_length);
        std::vector<char> shader_log(info_log_length);
        glGetShaderInfoLog(shader, info_log_length, NULL, &shader_log[0]);
        std::cout << "ERROR compiling shader: " << shaderName.c_str() << std::endl << &shader_log[0] << std::endl;
    }

    return shader;

}

void ShaderManager::CreateProgram(const std::string& shaderName, const std::string& vertexShaderFilename, const std::string& fragmentShaderFilename)
{
    /**
     * Creates actual shading program.
     * In the end appends program to programs map.
     *
     * @param shaderName id of shader program. Used to append program at certein id.
     * @param vertexShaderFilename filename of vertex .glsl code
     * @param fragmentShaderFilename filename of framgment .glsl code
     */
    //read the shader files and save the code
    std::string vertex_shader_code = ReadShader(vertexShaderFilename);
    std::string fragment_shader_code = ReadShader(fragmentShaderFilename);

    GLuint vertex_shader = CreateShader(GL_VERTEX_SHADER, vertex_shader_code, "vertex shader");
    GLuint fragment_shader = CreateShader(GL_FRAGMENT_SHADER, fragment_shader_code, "fragment shader");

    int  link_result = 0;
    //create the program handle, attach the shaders and link it
    GLuint program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);

    glLinkProgram(program);
    glGetProgramiv(program, GL_LINK_STATUS, &link_result);

    if (link_result == GL_FALSE)
    {

        int info_log_length = 0;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &info_log_length);
        std::vector<char> program_log(info_log_length);
        glGetProgramInfoLog(program, info_log_length, NULL, &program_log[0]);
        std::cout << "Shader Loader : LINK ERROR" << std::endl << &program_log[0] << std::endl;
        return;
    }

    programs[shaderName] = program;
}

const GLuint ShaderManager::GetShader(const std::string& shaderName)
{
    /**
     * Returns program of given shaderName from programs map.
     */
    return programs.at(shaderName);

}