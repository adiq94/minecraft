#include <Core/Texture/Cubemap.h>
#include <stdexcept>
#include <Core/Texture/Image.h>

GLuint Cubemap::LoadCubeMap(std::string right, std::string left, std::string top, std::string bottom, std::string back,
                          std::string front) {
    glGenTextures(1, &textureID);
    glActiveTexture(GL_TEXTURE0);
    Bind();

    auto frontImage = Image(front);
    auto rightImage = Image(right);
    auto leftImage = Image(left);
    auto topImage = Image(top);
    auto bottomImage = Image(bottom);
    auto backImage = Image(back);

    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA, rightImage.width, rightImage.height, 0, GL_RGBA,
                 GL_UNSIGNED_BYTE, rightImage.data);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA, leftImage.width, leftImage.height, 0, GL_RGBA,
                 GL_UNSIGNED_BYTE, leftImage.data);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA, topImage.width, topImage.height, 0, GL_RGBA,
                 GL_UNSIGNED_BYTE, topImage.data);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA, bottomImage.width, bottomImage.height, 0, GL_RGBA,
                 GL_UNSIGNED_BYTE, bottomImage.data);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA, backImage.width, backImage.height, 0, GL_RGBA,
                 GL_UNSIGNED_BYTE, backImage.data);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA, frontImage.width, frontImage.height, 0, GL_RGBA,
                 GL_UNSIGNED_BYTE, frontImage.data);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    Unbind();

    return textureID;
}

void Cubemap::Bind() {
    glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
}
void Cubemap::Unbind() {
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

Cubemap::Cubemap(std::string right, std::string left, std::string top, std::string bottom, std::string back,
                 std::string front) {
    LoadCubeMap(right,left,top,bottom,back,front);
}

Cubemap::Cubemap() {

}

GLuint Cubemap::GetTextureId() {
    return 0;
}
