#include <Core/Texture/Image.h>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <stdexcept>


void Image::Load(std::string filename) {
    data = stbi_load(filename.c_str(), &width, &height, &numberOfChannels, desiredChannels);
    if(data == nullptr) throw std::runtime_error("Could not load texture: " + filename);
}

Image::~Image() {
    if(data != nullptr) stbi_image_free(data);
}
