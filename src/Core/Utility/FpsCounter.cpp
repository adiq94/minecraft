#include <Core/Utility/FpsCounter.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <sstream>

void FpsCounter::showFPS(GLFWwindow *pWindow) {
    // Measure speed
    double currentTime = glfwGetTime();
    double delta = currentTime - lastTime;
    nbFrames++;
    if ( delta >= 1.0 ){ // If last cout was more than 1 sec ago
        fps = double(nbFrames) / delta;
        std::stringstream ss;
        ss << "Minecraft" << " [" << fps << " FPS]";

        glfwSetWindowTitle(pWindow, ss.str().c_str());

        nbFrames = 0;
        lastTime = currentTime;
    }
}
