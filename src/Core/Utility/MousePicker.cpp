//
// Created by Albert on 16.04.2017.
//

#include "Core/Utility/MousePicker.h"
#include <easylogging++.h>

MousePicker::MousePicker(glm::mat4 projection, glm::mat4 viewMatrix, int width, int heigth){
    this->projectionMatrix = projection;
    this->viewMatrix = viewMatrix;
    this->width = width;
    this->heigth = heigth;
}

glm::vec3 MousePicker::GetCurrentRay(){
    return this->currentRay;
}

glm::vec3 MousePicker::CalculateMouseRay() {
    float mouseX = this->width / 2;
    float mouseY = this->heigth / 2;

    glm::vec2 normalizedCoords = GetNormalizedCoords(mouseX, mouseY);
    glm::vec4 clipCoords = glm::vec4(normalizedCoords.x, normalizedCoords.y, -1.0f, 1.0f);
    glm::vec4 eyeCoords = ToEyeCoords(clipCoords);
    glm::vec3 worldRay = ToWorldCoords(eyeCoords);
    return worldRay;
}

void MousePicker::update() {
    currentRay = CalculateMouseRay();
    LOG_EVERY_N(60, INFO) << "x: " << currentRay.x << " y: " << currentRay.y << " z: " << currentRay.z;
}

glm::vec2 MousePicker::GetNormalizedCoords(float mouseX, float mouseY) {
    float x = (2.0f*mouseX) / this->width - 1.0f;
    float y = (2.0f*mouseY) / this->heigth - 1.0f;
    return glm::vec2(x, -y);
}

glm::vec4 MousePicker::ToEyeCoords(glm::vec4 clipCoords) {
    glm::mat4 invertedProjection = glm::inverse(projectionMatrix);
    glm::vec4 eyeCoorts = invertedProjection * clipCoords;
    return glm::vec4(eyeCoorts.x, eyeCoorts.y, -1.0f, 0.0f);
}

glm::vec3 MousePicker::ToWorldCoords(glm::vec4 eyeCoords) {
    glm::mat4 invertedView = glm::inverse(viewMatrix);
    glm::vec4 rayWorld = invertedView * eyeCoords;
    glm::vec3 mouseRay = glm::vec3(rayWorld.x, rayWorld.y, rayWorld.z);
    mouseRay = glm::normalize(mouseRay);
    return mouseRay;
}
