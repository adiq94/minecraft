#include <Core/Utility/DebugDrawer.h>
#include <Core/Model/Line.h>
#include <Core/Manager/ShaderManager.h>
#include <Core/Model/DebugLine.h>

void DebugDrawer::update() {
    auto name = "debug_lines";
    DebugLine *line = new DebugLine();
    line->SetProgram(ShaderManager::GetShader("lineShader"));
    if(initiated){
        modelManager->DeleteModel(name);
    }

    line->Create(vertices, colors);
    modelManager->SetModel(name, line);
    vertices.clear();
    colors.clear();
    initiated = true;
}
