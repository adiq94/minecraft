#include <Core/Application/Application.h>
#include <easylogging++.h>
#include <Core/Physics/HelloWorld.h>

INITIALIZE_EASYLOGGINGPP

int main(void) {
    /**
     * Main file.
     *
     * Creates Application, returns Run() method.
     * Initiation of Logger.
     */
    el::Configurations conf("logging.conf");
    el::Loggers::reconfigureAllLoggers(conf);
    auto app = Application();
    return app.Run();
}