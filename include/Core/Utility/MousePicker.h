//
// Created by Albert on 16.04.2017.
//

#ifndef MINECRAFT_MOUSEPICKER_H
#define MINECRAFT_MOUSEPICKER_H


#include <glm/glm.hpp>
#include <Core/Model/Camera.h>
#include <glm/gtc/matrix_transform.hpp>

class MousePicker {

    public:
        MousePicker(glm::mat4 projection, glm::mat4 viewMatrix, int width, int heigth);

        glm::vec3 GetCurrentRay();
        glm::vec3 CalculateMouseRay();   // main calulations and transformations
        glm::vec2 GetNormalizedCoords(float mouseX, float mouseY);
        glm::vec4 ToEyeCoords(glm::vec4 clipCoords);
        glm::vec3 ToWorldCoords(glm::vec4 eyeCoords);

        void update();

    private:
        int width;
        int heigth;
        glm::vec3 currentRay;
        glm::mat4 projectionMatrix;
        glm::mat4 viewMatrix;

};


#endif //MINECRAFT_MOUSEPICKER_H
