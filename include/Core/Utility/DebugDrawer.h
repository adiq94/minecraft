#ifndef MINECRAFT_DEBUGDRAWER_H
#define MINECRAFT_DEBUGDRAWER_H

#include <LinearMath/btIDebugDraw.h>
#include <easylogging++.h>
#include <Core/Manager/ModelManager.h>
#include <Core/Model/Line.h>

class DebugDrawer : public btIDebugDraw{
public:
    DebugDrawer(ModelManager *manager) {
        modelManager = manager;
    }

    virtual ~DebugDrawer() {

    }

    void drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &color) override {
        vertices.push_back(from.getX());
        vertices.push_back(from.getY());
        vertices.push_back(from.getZ());
        vertices.push_back(to.getX());
        vertices.push_back(to.getY());
        vertices.push_back(to.getZ());

        colors.push_back(color.getX());
        colors.push_back(color.getY());
        colors.push_back(color.getZ());
        colors.push_back(1);
        colors.push_back(color.getX());
        colors.push_back(color.getY());
        colors.push_back(color.getZ());
        colors.push_back(1);
    }

    void drawContactPoint(const btVector3 &PointOnB, const btVector3 &normalOnB, btScalar distance, int lifeTime,
                          const btVector3 &color) override {

    }

    void reportErrorWarning(const char *warningString) override {
        LOG(WARNING) << warningString;
    }

    void draw3dText(const btVector3 &location, const char *textString) override {

    }

    void setDebugMode(int debugMode) override {

    }

    int getDebugMode() const override {
        return btIDebugDraw::DBG_DrawWireframe  | btIDebugDraw::DBG_DrawAabb | btIDebugDraw::DBG_DrawNormals | btIDebugDraw::DBG_DrawFrames  ;
    }

    void update();

private:
    ModelManager *modelManager;
    std::vector<float> vertices;
    std::vector<float> colors;
    bool initiated = false;
};
#endif //MINECRAFT_DEBUGDRAWER_H
