#ifndef MINECRAFT_FPSCOUNTER_H
#define MINECRAFT_FPSCOUNTER_H

#include <GLFW/glfw3.h>

class FpsCounter{
public:
    void showFPS(GLFWwindow *pWindow);

    int nbFrames = 0;
    double lastTime = 0;
    double fps = 0;
};
#endif //MINECRAFT_FPSCOUNTER_H
