#ifndef MINECRAFT_SHADERMANAGER_H
#define MINECRAFT_SHADERMANAGER_H

#include <glad/glad.h>
#include <map>

class ShaderManager
{

public:

    ShaderManager(void);
    ~ShaderManager(void);

    void CreateProgram(const std::string& shaderName,
                       const std::string& VertexShaderFilename,
                       const std::string& FragmentShaderFilename);

    static const GLuint GetShader(const std::string&);

private:

    std::string ReadShader(const std::string& filename);
    GLuint CreateShader(GLenum shaderType,
                        const std::string& source,
                        const std::string& shaderName);

    static std::map<std::string, GLuint> programs;
};
#endif //MINECRAFT_SHADERMANAGER_H
