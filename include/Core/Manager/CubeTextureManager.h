//
// Created by Albert on 15.04.2017.
//

#ifndef MINECRAFT_TEXTUREMANAGER_H
#define MINECRAFT_TEXTUREMANAGER_H

#include <vector>

struct AtlasIndex{
    AtlasIndex(int x, int y);

    int x;
    int y;
};


class CubeTextureManager{

    public:
        std::vector<float> GetTextureCoordsFromSource(int size, std::vector<AtlasIndex> indexes);

    private:
    int itemCount = 2;

};


#endif //MINECRAFT_TEXTUREMANAGER_H
