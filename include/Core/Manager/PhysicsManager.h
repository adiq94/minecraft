#ifndef MINECRAFT_PHYSICSMANAGER_H
#define MINECRAFT_PHYSICSMANAGER_H

#include <BulletDynamics/Dynamics/btDiscreteDynamicsWorld.h>
#include <BulletCollision/CollisionDispatch/btDefaultCollisionConfiguration.h>
#include <BulletDynamics/ConstraintSolver/btSequentialImpulseConstraintSolver.h>
#include <map>
#include <glm/vec3.hpp>
#include <BulletDynamics/Character/btKinematicCharacterController.h>
#include <Core/Utility/DebugDrawer.h>
#include "ModelManager.h"

class PhysicsManager {
public:
    void Init(ModelManager *manager);
    void Update(float frameRate);
    btRigidBody *CreateCube(glm::vec3 position);
    std::map<btRigidBody*,glm::vec3> GetMap();
    void AddPlayer();
    virtual ~PhysicsManager();
    btKinematicCharacterController *GetPlayerController();
    btPairCachingGhostObject *GetGhost();
    btDiscreteDynamicsWorld *GetWorld();
    DebugDrawer *GetDebugDrawer();

private:
    btDiscreteDynamicsWorld* dynamicsWorld;
    btAlignedObjectArray<btCollisionShape*> collisionShapes;
    btDefaultCollisionConfiguration* collisionConfiguration;
    ///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
    btCollisionDispatcher* dispatcher;
    ///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
    btBroadphaseInterface* overlappingPairCache;
    ///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
    btSequentialImpulseConstraintSolver* solver;
    std::map<btRigidBody*,glm::vec3> PhysicsPositions;
    btKinematicCharacterController *playerController;
    btPairCachingGhostObject *ghost;
    DebugDrawer *drawer;
};
#endif //MINECRAFT_PHYSICSMANAGER_H
