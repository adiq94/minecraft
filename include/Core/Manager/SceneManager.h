#ifndef MINECRAFT_SCENEMANAGER_H
#define MINECRAFT_SCENEMANAGER_H
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <Core/Model/Camera.h>
#include "IListener.h"
#include "ModelManager.h"
#include "ShaderManager.h"
#include "PhysicsManager.h"

static glm::mat4 view_matrix;
static Camera *camera;
static float widthForCamera;
static float heightForCamera;
static PhysicsManager* physics_manager;
class SceneManager : public IListener

{
public:
    SceneManager(float width, float height);
    ~SceneManager();

    virtual void NotifyBeginFrame(float framerate);
    virtual void NotifyDisplayFrame();
    virtual void NotifyEndFrame();
    virtual void NotifyReshape(int width, int height, int previos_width, int previous_height);
    static void KeyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
    static void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
    static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos);

    ModelManager* GetModels_Manager();
    PhysicsManager* GetPhysicsManager();

private:
    ShaderManager* shader_manager;
    ModelManager* models_manager;
    glm::mat4 projection_matrix;

    static void SetWalkDirection(btKinematicCharacterController *controller, btPairCachingGhostObject *ghost);
};
#endif //MINECRAFT_SCENEMANAGER_H
