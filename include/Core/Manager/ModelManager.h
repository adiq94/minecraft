#ifndef MINECRAFT_MODELMANAGER_H
#define MINECRAFT_MODELMANAGER_H
#include <glm/matrix.hpp>
#include <Core/Model/IEntity.h>
#include <map>
#include <string>
#include <sstream>
#include <iostream>
#include <BulletDynamics/Dynamics/btRigidBody.h>

class ModelManager
{
public:
    ModelManager();
    ~ModelManager();

    void Draw();
    void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix);
    void Update(std::map<btRigidBody*,glm::vec3> map);

    void DeleteModel(const std::string& gameModelName);
    const IEntity& GetModel(const std::string& gameModelName) const;

    void DeleteModel_NDC(const std::string& gameModelName);
    const IEntity& GetModel_NDC(const std::string& gameModelName) const;

    void SetModel(const std::string& gameObjectName, IEntity* gameObject);

private:
    std::map<std::string, IEntity*> gameModelList;
    std::map<std::string, IEntity*> gameModelList_NDC;
};
#endif //MINECRAFT_MODELMANAGER_H
