#ifndef MINECRAFT_CUBEMAP_H
#define MINECRAFT_CUBEMAP_H

#include <string>
#include <glad/glad.h>

class Cubemap {
public:
    Cubemap(std::string right, std::string left, std::string top, std::string bottom, std::string back,
            std::string front);
    Cubemap();
    GLuint LoadCubeMap(std::string right, std::string left, std::string top, std::string bottom, std::string back,
                             std::string front);
    void Bind();
    void Unbind();
    GLuint GetTextureId();
private:
    GLuint textureID;
};

#endif //MINECRAFT_CUBEMAP_H
