
#ifndef MINECRAFT_TEXTURE_H
#define MINECRAFT_TEXTURE_H
#include<string>
#include <glad/glad.h>
#include <Core/Texture/Image.h>

class Texture{
public:
    Texture(std::string filename);
    ~Texture();

    void Load(std::string filename);
    void bind();
    void unbind();
    Image *image;
private:
    GLuint textureId;
};
#endif //MINECRAFT_TEXTURE_H
