#ifndef MINECRAFT_IMAGE_H
#define MINECRAFT_IMAGE_H
#include <string>

class Image {
public:
    Image(std::string filename) {
        Load(filename);
    }

    virtual ~Image();

    Image(){}
    void Load(std::string filename);
    unsigned char* data;
    int width;
    int height;
    int numberOfChannels;
    int desiredChannels = 4;
};
#endif //MINECRAFT_IMAGE_H
