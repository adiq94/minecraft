#ifndef MINECRAFT_IENTITY_H
#define MINECRAFT_IENTITY_H

#include <glad/glad.h>
#include <vector>
#include <glm/matrix.hpp>
#include <BulletDynamics/Dynamics/btRigidBody.h>
#include <map>

class IEntity {
public:
    virtual ~IEntity() = 0;

    virtual void Draw() = 0;

    virtual void Draw(const glm::mat4 &projectionMatrix, const glm::mat4 &viewMatrix) = 0;

    virtual void Update(std::map<btRigidBody *, glm::vec3>) = 0;

    virtual void SetProgram(GLuint shaderName) = 0;

    virtual void Destroy() = 0;

    virtual GLuint GetVao() const = 0;

    virtual const std::vector<GLuint> GetVbos() const = 0;

};

inline IEntity::~IEntity() {
}

#endif //MINECRAFT_IENTITY_H
