#ifndef MINECRAFT_CUBE_H
#define MINECRAFT_CUBE_H

#include "Model.h"
#include <string>
#include <BulletDynamics/Dynamics/btRigidBody.h>

class Cube : public Model{
public:
    Cube();

    virtual ~Cube();

    void Draw(const glm::mat4 &projection_matrix, const glm::mat4 &view_matrix) override;

    void Update(std::map<btRigidBody *, glm::vec3> map) override;

    void Create(btRigidBody *body);
    glm::vec3 position;
private:
    void CheckError(std::string name);
    btRigidBody *body;
};
#endif //MINECRAFT_CUBE_H
