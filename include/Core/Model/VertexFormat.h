#ifndef MINECRAFT_VERTEXFORMAT_H
#define MINECRAFT_VERTEXFORMAT_H

#include <glm/vec4.hpp>
#include <glm/vec3.hpp>

struct VertexFormat
	{
		glm::vec3 position;
		glm::vec4 color;

		VertexFormat(const glm::vec3 &pos, const glm::vec4 &col)
		{
			position = pos;
			color = col;
		}
		VertexFormat()
		{

		}

	};
#endif //MINECRAFT_VERTEXFORMAT_H
