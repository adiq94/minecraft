#ifndef MINECRAFT_DEBUGLINE_H
#define MINECRAFT_DEBUGLINE_H

#include "Model.h"

class DebugLine : public Model{
public:
    void Create(std::vector<float> vertices, std::vector<float> colors);

    void Draw(const glm::mat4 &projection_matrix, const glm::mat4 &view_matrix) override;
private:
    std::vector<float> vertices;
    std::vector<float> colors;
};

#endif //MINECRAFT_DEBUGLINE_H
