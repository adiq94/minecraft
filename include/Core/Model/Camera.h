#ifndef MINECRAFT_CAMERA_H
#define MINECRAFT_CAMERA_H

#include <glm/detail/type_mat4x4.hpp>
#include <glm/gtc/constants.hpp>
class Camera {

public:
    Camera();
    void UpdateView();

    glm::mat4 viewMatrix;
    float roll = 0;
    float pitch = glm::quarter_pi<float>();
    float yaw = 1.5 * glm::pi<float>();
    glm::vec3 cameraPositionEyeVector;

    glm::mat4 GetViewMatrix() const;

    void MouseMove(float x, float y, float width, float height);

    glm::vec2 mousePosition;

    void KeyPressed(const unsigned char key);
};
#endif //MINECRAFT_CAMERA_H
