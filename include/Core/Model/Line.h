#ifndef MINECRAFT_LINE_H
#define MINECRAFT_LINE_H

#include "Model.h"
#include <string>
#include <functional>

class Line : public Model{
public:
    void Create(std::function<void(Line* line)> createAction, std::function<void(Line* line)> drawAction);
    void Draw(const glm::mat4 &projection_matrix, const glm::mat4 &view_matrix) override;
    glm::mat2x3 vertices;
    glm::mat2x4 colors;
    glm::mat2x3 translatedVertices;
private:
    std::function<void(Line* line)> drawAction;
};
#endif //MINECRAFT_LINE_H
