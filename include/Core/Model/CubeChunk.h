#ifndef MINECRAFT_CUBE_CHUNK_H
#define MINECRAFT_CUBE_CHUNK_H

#include "Model.h"
#include <string>

class CubeChunk : public Model
{
public:
    CubeChunk();
    ~CubeChunk();
    void Create(int length, int width, int height, glm::vec3 offset);
    virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix) override final;
private:

    void CheckError(std::string name);
    int length,width,height;
    glm::vec3 offset;
};
#endif //MINECRAFT_CUBE_CHUNK_H
