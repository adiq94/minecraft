#ifndef MINECRAFT_MODEL_H
#define MINECRAFT_MODEL_H

#include <easylogging++.h>
#include "IEntity.h"

class Model : public IEntity {

public:
    Model();

    virtual ~Model();

    virtual void Draw() override;

    virtual void Draw(const glm::mat4 &projection_matrix, const glm::mat4 &view_matrix) override;

    virtual void Update(std::map<btRigidBody *, glm::vec3>) override;

    virtual void SetProgram(GLuint shaderName) override;

    virtual void Destroy() override;

    virtual GLuint GetVao() const override;

    virtual const std::vector<GLuint> GetVbos() const override;

    void CheckError(std::string name) {
        auto er = glGetError();
        if(er != GL_NO_ERROR){
            LOG(INFO) << name << ": " << er;
        }
    }

protected:
    GLuint vao;
    GLuint program;
    std::vector<GLuint> vbos;
};

#endif //MINECRAFT_MODEL_H
