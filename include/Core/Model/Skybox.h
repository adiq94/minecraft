#ifndef MINECRAFT_SKYBOX_H
#define MINECRAFT_SKYBOX_H

#include <string>
#include <Core/Texture/Cubemap.h>
#include "Model.h"

class Skybox : public Model{
public:
    void Create();
    void Draw(const glm::mat4 &projection_matrix, const glm::mat4 &view_matrix) override;
private:
    GLuint skyboxVBO;
    Cubemap *skybox;
    std::string GetTextureLocation(std::string name);

    void CheckError(std::string name);
};
#endif //MINECRAFT_SKYBOX_H
