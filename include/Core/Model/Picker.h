//
// Created by Albert on 16.04.2017.
//

#ifndef MINECRAFT_PICKER_H
#define MINECRAFT_PICKER_H


#include "Model.h"

class Picker : public Model {
public:
    void Draw(const glm::mat4 &projection_matrix, const glm::mat4 &view_matrix) override;
};


#endif //MINECRAFT_PICKER_H
