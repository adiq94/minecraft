
#ifndef MINECRAFT_APPLICATION_H
#define MINECRAFT_APPLICATION_H
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <Core/Manager/IListener.h>
#include <string>
#include <Core/Manager/SceneManager.h>
#include <Core/Model/Line.h>

class Application
{
public:
    Application();

public:
    int height;
    int width;
    int v_sync;
    int AA_value;
    std::string title;

    int Run(); // Run main game loop
private:
    void Update();
    GLFWwindow *GetWindow(int width, int height, const char *title);
    GLFWwindow *window;
    void SetListener(IListener* listener);
    IListener *listener;

    void AddChunks(SceneManager *scene, int x, int y);
    void AddSkybox(SceneManager *scene);
    void AddPicker(SceneManager *scene);
    void AddLine(SceneManager *scene, std::function<void(Line *line)> createAction,
                 std::function<void(Line *line)> drawAction, std::string name);
    void AddLineX(SceneManager *scene);
    void AddLineY(SceneManager *scene);
    void AddLineZ(SceneManager *scene);
    void AddCube(SceneManager *scene, int x, int y, int z, std::string name);
//    static void ResizeWindow(GLFWwindow *window, int width, int heigth);
};
#endif //MINECRAFT_APPLICATION_H
