#version 330 core

layout(location = 0) out vec4 outTextureCoords;

in vec2 passTextureCoords;
uniform sampler2D ourTexture;


void main()
{
 	outTextureCoords = texture(ourTexture, passTextureCoords);
}