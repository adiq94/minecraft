#version 330 core
layout(location = 0) in vec3 in_position;
layout (location = 1) in vec2 inTextureCoords;
layout(location = 2) in vec3 in_offset;

uniform mat4 projection_matrix, view_matrix;
uniform vec3 rotation;

out vec2 passTextureCoords;

void main()
{
	gl_Position = projection_matrix * view_matrix * vec4(in_position + in_offset, 1);
	passTextureCoords = inTextureCoords;
}